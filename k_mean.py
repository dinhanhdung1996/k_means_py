import math
import random


class Point:

    def __init__(self, x=None, y=None):
        self.x = x
        self.y = y
        self.id = -1

    def generate(self, n):
        self.x = random.randrange(0, int(n))
        self.y = random.randrange(0, int(n))

    def distance_to(self, point):
        if self.x is None or self.y is None:
            return
        if type(point) is not Point:
            return None
        if self.x is None or self.y is None:
            return None
        return math.sqrt(math.pow(self.x - point.x, 2) + math.pow(self.y - point.y, 2))

    def equals(self, point):
        if type(point) is not Point:
            return False
        if self.x == point.x and self.y == point.y:
            return True
        return False

    def __str__(self):
        if self.id != -1:
            return str(self.id) + "-(" + str(self.x) + "," +str(self.y) + ")"
        else:
            return "(" + str(self.x) + "," + str(self.y) + ")"


class Cluster(Point):

    def __init__(self, x=None, y=None):
        Point.__init__(self, x, y)
        self.points = []

    def add_point(self, point):
        if type(point) is not Point:
            return
        self.points.append(point)

    def get_new_pos(self):
        sum_x = 0
        sum_y = 0
        for point in self.points:
            sum_x += point.x
            sum_y += point.y
        new_x = sum_x/len(self.points)
        new_y = sum_y/len(self.points)
        self.x = new_x
        self.y = new_y

    def __str__(self):
        re_str = "["
        for a in self.points:
            re_str += str(a)
            re_str += ","
        re_str += "]"
        return re_str


class KMean:

    def __init__(self, n=2,m=10):
        self.points = []
        self.clusters = []
        self.number_clusters = n
        self.number_points = m

    def generate_data(self):
        for i in range(self.number_clusters):
            a = Cluster()
            a.generate(100)
            self.clusters.append(a)
        for i in range(self.number_points):
            a = Point()
            a.generate(100)
            self.points.append(a)

    def generate_cluster(self):
        for i in range(self.number_clusters):
            a = Cluster()
            a.generate(100)
            self.clusters.append(a)

    def get_in_data(self, file_name):
        f = open(file_name)
        for line in f:
            str_list = line.split(" ")
            point = Point()
            point.id = int(str_list[0])
            point.x = int(str_list[1])
            point.y = int(str_list[2])
            self.points.append(point)

    def k_mean(self):
        for point in self.points:
            min_cluster = self.clusters[0]
            min_dis = point.distance_to(min_cluster)
            for cluster in self.clusters:
                if point.distance_to(cluster) < min_dis:
                    min_cluster = cluster
                    min_dis = point.distance_to(cluster)
            min_cluster.add_point(point)

    def re_kmean(self):
        for cluster in self.clusters:
            cluster.get_new_pos()
        self.k_mean()

